# Try to import PyQt5, otherwise use PyQt4
try:
    __import__('PyQt5')
    use_pyqt5 = True
except ImportError:
    use_pyqt5 = False

if use_pyqt5:
    from PyQt5.QtWidgets import QMainWindow, QGridLayout, QWidget
else:
    from PyQt4.QtGui import QMainWindow, QGridLayout, QWidget

from signal_number import SignalNumber
from config import number_signals


class Window(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.resize(600, 300)
        self.grid = QGridLayout()
        self.setStyleSheet("background-color: black;")
        widget = QWidget()
        self.setCentralWidget(widget)
        self.grid.setSpacing(10)
        widget.setLayout(self.grid)
        self.signals = self.add_signals()

    def resizeEvent(self, event):
        for name, label in self.signals.items():
            label.set_rel_label_size(self.height(), self.width())

    # numeric and graphical signals with main properties
    def add_signals(self):
        signals =  {}
        for signal, prefs in number_signals.items():
            signals[signal] = SignalNumber(prefs["label"], prefs["color"], *prefs["pos"], prefs["size"], self.grid)
        return signals
