chart_colors = {
    "green": [0, 1, 0],
    "lightblue": [0.9, 0.9, 1],
    "red": [1, 0, 0],
}

chart_settings = {
    "canvas_pos": [0, 0, 13, 5],
    "x_axis_length": 3000
}

# y_corr defines the vertical position
chart_signals = {
    'ECG0': { "color": chart_colors["green"], "y_corr": 5, "y_range": [3, 3.5] },
    'ECG1': { "color": chart_colors["green"], "y_corr": 1, "y_range": [0, 0.8] },
    'spo2': { "color": chart_colors["lightblue"], "y_corr": -2, "y_range": [97, 99] },
    'pulse': { "color": chart_colors["red"], "y_corr": -5, "y_range": [-1, 1] },
}

# pos array holds [y_pos, x_pos, y_span, x_span]
number_signals = {
    'ECG0': { "label": "ECG", "color": "green", "pos": [2, 5, 4, 2], "size": 2.5 },
    'spo2': { "label": "spo2", "color": "lightblue", "pos": [6, 5, 3, 1], "size": 1 },
    'pulse': { "label": "Pulse", "color": "red", "pos": [6, 6, 3, 1], "size": 1 },
    'BPSYS': { "label": "SYS", "color": "pink", "pos": [9, 5, 3, 1], "size": 1 },
    'BPDIA': { "label": "DIA", "color": "pink", "pos": [9, 6, 3, 1], "size": 1 },
}
