import numpy as np
from vispy import scene


class SignalGraph(scene.visuals.Line):
    def __init__(self, view, y_range, y_corr, *args, **kwargs):
        self.min, self.max = y_range
        self.y_corr = y_corr
        self.view = view

        self.last_x_pos = 0
        # Create some initial points [[0, y_corr], [0, y_corr], [0, y_corr]]
        zeros = np.zeros((3, 2), dtype=np.float32)
        zeros[:, 1] = 0.0 + self.y_corr
        # add line and make it editable
        scene.Line.__init__(self, pos=zeros, *args, **kwargs)
        self.unfreeze()
        self.view.add(self)
        # add line with history from last runthrough
        self.shadow_line = scene.Line(pos=zeros, color=np.append(kwargs['color'], 0.7))
        self.view.add(self.shadow_line)

    # continue drawing line from left
    def reset(self, first_value):
        # two-dimensional array with zero values
        # since initial line needs three points minimum
        first_pos = np.zeros((3, 2), dtype=np.float32)
        # shift starting point to first value
        first_pos[:, 1] = first_value
        self._pos = first_pos

    # let shadow line shrink from left as current line enlarges
    def cut_shadow_line(self, new_x):
        # gap between shadow line and current line
        gap_value = 0.9
        positions = self.shadow_line._pos
        if len(positions) > 0:
            index = 0
            # search for first point on shadow line which is larger
            # than the current running value (+ gap_value) and save its index
            for shadow_x in positions[0]:
                index += 1
                if shadow_x > (new_x + gap_value):
                    break
            # cut out positions between index and end of line
            cutted_pos = positions[index:self.canvas.get_x_length()-1]
            self.shadow_line._pos = cutted_pos

    # add point from incoming stream data
    def add_point(self, new_data):
        time, value = new_data
        # Use the modulus in order to continue
        # line from left after x_axis_length exceeded
        pos_x = time % self.canvas.get_x_length()
        normalized = (value - self.min) / (self.max - self.min) + self.y_corr

        # unless end of x-axis reached, attach point to current line and cut shadow line,
        if pos_x >= self.last_x_pos:
            scaled_x = pos_x / (self.canvas.get_x_length() / 28) # scale x axis to viewport width
            self.cut_shadow_line(scaled_x)
            self._pos = np.append(self.pos, [[scaled_x, normalized]], axis=0)
        else:
            self.shadow_line._pos = self._pos
            self.reset(normalized)
        self.last_x_pos = pos_x
