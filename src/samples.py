import sys
import struct
import select


# Reads STDIN binary buffer
class Samples:
    def __init__(self, files=None):
        self.files = []
        if not files:
            self.files.append(sys.stdin.buffer)
        else:
            for f in files:
                self.files.append(open(f,"rb"))
    def __iter__(self):
        while True:
            readables,_,_ = select.select(self.files,[],[],0)
            if not readables:
                return
            for readable in readables:
                length, = struct.unpack("H", readable.read(2))
                buf     = readable.read(length)
                timestamp, value, name, _, _ = struct.unpack(f"Qf{length-14}scc", buf)
                yield [name.decode("utf-8"), timestamp, value]
