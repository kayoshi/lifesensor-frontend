from vispy_canvas import Canvas
from qt_window import Window
from samples import Samples
import vispy.app
import random
from config import chart_settings


vispy_canvas = Canvas()
qt_window = Window()
qt_window.grid.addWidget(vispy_canvas.native, *chart_settings["canvas_pos"])

# Trigger canvas resize function
@vispy_canvas.events.resize.connect
def resize(event=None):
    vispy_canvas.resize()
resize()

samples = Samples()

# Get sample data from STDIN. Samples() delivers all data gathered
# while last iteration was processed (drawing graph and number)
def get_samples(event):
    # update lines and text
    for n, t, v in samples:
        for name, graph in vispy_canvas.signals.items():
            if n == name:
                graph.add_point([t, v])
                break
        for name, text in qt_window.signals.items():
            if n == name:
                text.number.setText(str(round(v, 2)))
                break

    # update canvas and print FPS to terminal output
    vispy_canvas.update()
    vispy_canvas.measure_fps()

# Run endless loop
timer1 = vispy.app.Timer('auto', connect=get_samples, start=True)

qt_window.show()
vispy.app.run()
