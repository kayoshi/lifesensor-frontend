import numpy as np
from vispy import scene, app
from signal_graph import SignalGraph
from config import chart_signals, chart_settings

class Canvas(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, keys='interactive')
        # make canvas editable
        self.unfreeze()
        # milliseconds between each canvas drawing update
        # milliseconds on x axis
        self.x_length = chart_settings["x_axis_length"]
        # camera
        self.view = self.central_widget.add_view()
        self.view.camera = scene.PanZoomCamera(rect=(0, 0, 30, 1), aspect=1.0)
        # add graphs and numerical signals
        self.signals = self.add_signals()
        # settings
        # self.show()
        self.freeze()

    # numeric and graphical signals with main properties
    def add_signals(self):
        signals =  {}
        for signal, prefs in chart_signals.items():
            signals[signal] = self.add_graph(color=prefs["color"], y_corr=prefs["y_corr"], y_range=prefs["y_range"])
        return signals

    def add_graph(self, **kwargs):
        graph = SignalGraph(view=self.view, **kwargs)
        return graph

    def resize(self):
        print('resize')

    def get_x_length(self):
        return self.x_length

    def on_key_press(self, event):
        if event.key in ['Right']:
            self.x_length += 500
        if event.key in ['Left']:
            if self.x_length > 500:
                self.x_length -= 500
