# Try to import PyQt5, otherwise use PyQt4
try:
    __import__('PyQt5')
    use_pyqt5 = True
except ImportError:
    use_pyqt5 = False

if use_pyqt5:
    from PyQt5.QtWidgets import QLabel
    from PyQt5 import QtGui
else:
    from PyQt4.QtGui import QLabel
    from PyQt4 import QtGui


class SignalNumber:
    def __init__(self, text, color, row, col, row_span, col_span, size_factor, grid):
        self.grid = grid
        self.color = color
        self.size_factor = size_factor

        self.number = self.create_label('-')
        self.grid.addWidget(self.number, row, col, row_span, col_span)

        self.label = self.create_label(text)
        # app crashs if y position is below 0
        vertical_label_offset = row if row - size_factor < 0 else size_factor
        self.grid.addWidget(self.label, row - vertical_label_offset, col, row_span, col_span)

    def create_label(self, text):
        label = QLabel(text)
        pal = label.palette()
        pal.setColor(QtGui.QPalette.WindowText, QtGui.QColor(self.color))
        label.setPalette(pal)
        return label

    def set_rel_label_size(self, height, width):
        self.label.setFixedSize(width / 10, width / 40)
        size = str(round(width / 50))
        self.label.setStyleSheet("font-weight: bold; font-size: " + size + "px")

        self.number.setFixedSize(width / 10 * self.size_factor, width / 20 * self.size_factor)
        size = str(round(width / 25 * self.size_factor))
        self.number.setStyleSheet("font-weight: bold; font-size: " + size + "px")
