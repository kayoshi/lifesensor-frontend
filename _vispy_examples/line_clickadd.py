import numpy as np

from vispy import app, scene


class EditLineVisual(scene.visuals.Line):
    def __init__(self, *args, **kwargs):
        scene.visuals.Line.__init__(self, *args, **kwargs)

    def on_mouse_press(self, pos_scene):
        # if no point was clicked add a new one
        print([pos_scene[:3]])
        self._pos = np.append(self.pos, [pos_scene[:3]], axis=0)
        self.set_data(pos=self.pos)


class Canvas(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, keys='interactive', size=(800, 800))

        # Create some initial points
        n = 7
        self.unfreeze()
        self.pos = np.zeros((n, 3), dtype=np.float32)
        self.pos[:, 0] = np.linspace(-50, 50, n)
        self.pos[:, 1] = np.random.normal(size=n, scale=10, loc=0)

        # create new editable line
        self.line = EditLineVisual(pos=self.pos, color='w', width=3,
                                   antialias=True, method='gl')

        self.view = self.central_widget.add_view()
        self.view.camera = scene.PanZoomCamera(rect=(-100, -100, 200, 200),
                                               aspect=1.0)


        self.view.add(self.line)
        self.show()
        scene.visuals.GridLines(parent=self.view.scene)
        self.freeze()

    def on_mouse_press(self, event):
        tr = self.scene.node_transform(self.line)
        pos = tr.map(event.pos)
        print(pos)
        self.line.on_mouse_press(pos)



if __name__ == '__main__':
    win = Canvas()
    app.run()
