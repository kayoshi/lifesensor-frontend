from PyQt5.QtWidgets import *
from PyQt5 import QtCore
import vispy.app
import sys
import random

canvas = vispy.app.Canvas()
w = QMainWindow()
widget = QWidget()
w.setCentralWidget(widget)

grid = QGridLayout()
grid.setSpacing(10)
widget.setLayout(grid)

vis = canvas.native
label1 = QLabel("bla 1")
label2 = QLabel("bla 2")
label3 = QLabel("bla 3")
label4 = QLabel("bla 4")

grid.addWidget(vis,0,0,2,5);
grid.addWidget(label1,0,5,1,1)
grid.addWidget(label2,0,6,1,1)
grid.addWidget(label3,1,5,1,1)
grid.addWidget(label4,1,6,1,1)

def update_label():
    l = random.choice([label1, label2, label3, label4])
    l.setText(str(random.random()))

timer = QtCore.QTimer()
timer.timeout.connect(update_label)
timer.start(100)  # every 10,000 milliseconds


w.show()
vispy.app.run()
