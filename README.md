# LifeSensor Frontend
**Part of the Cadus LifeSensor project ([https://github.com/cadus/Life-Sensor](https://github.com/cadus/Life-Sensor))**

This project is meant to provide an open-source lightweight and performant visualization for human vital parameters, as ECG, pulse or oxygen supply. It is based on Python and QT using OpenGL as graphics accelerator.

## Get started
You need to install python (tested with version 3.8.5), its vispy library and QT. It has been tested on Ubuntu with QT4 on Ubuntu and Rasperry Pi OS Buster. For the later, you can follow the installation instructions on the vispy website: http://vispy.org/raspberry.html.

On Ubuntu, dependencies have been installed via `apt-get install python3-pyqt5 python3-pyqt5.qtopengl`
To check, if QT is running properly in python, use `import vispy; print(vispy.sys_info())` and hope for something like
```
pyqt4:    None
pyqt5:    ('PyQt5', '5.15.1', '5.15.1')
pyside:   None
pyside2:  None
```

Install requirements via `pip3 install -r requirements.txt`

To run the app with simulated data stream run `bash run.sh` or `python3 utils/stream_replay.py -b data/3chECG_bpsysdia_pulse_spo2.bin | python3 src/main.py`
`
## Screenshot
![Bildschirmfoto_von_2020-11-12_08-04-23](/uploads/e3960cb125f0483cca952234d0bf6358/Bildschirmfoto_von_2020-11-12_08-04-23.png)